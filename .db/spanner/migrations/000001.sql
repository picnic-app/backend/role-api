CREATE TABLE Roles
(
    ID          STRING(36) NOT NULL,
    Name        STRING(50) NOT NULL,
    Permissions JSON,
    CreatedAt   TIMESTAMP NOT NULL OPTIONS (allow_commit_timestamp = true),
    UpdatedAt   TIMESTAMP OPTIONS (allow_commit_timestamp = true),
    DeletedAt   TIMESTAMP OPTIONS (allow_commit_timestamp = true),
) PRIMARY KEY (ID);

CREATE TABLE AssignedRoles
(
    ID        STRING(36) NOT NULL,
    EntityID  STRING(36) NOT NULL,
    RoleID    STRING(36) NOT NULL,
    CircleID  STRING(36) NOT NULL,
    UserID    STRING(36) NOT NULL,
    CreatedAt TIMESTAMP NOT NULL OPTIONS (allow_commit_timestamp = true),
    CONSTRAINT FK_AssignedRoles_RoleID FOREIGN KEY (RoleID) REFERENCES Roles (ID)
) PRIMARY KEY (ID);