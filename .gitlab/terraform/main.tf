locals {
  project    = module.project.project
  is_not_dev = contains(["stg", "prod"], var.env_id)
}

module "project" {
  source  = "gitlab.com/picnic-app/project/google"
  version = "~> 1.0"

  env_id = var.env_id
}
