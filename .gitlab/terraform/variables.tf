variable "env_id" {
  type        = string
  description = "ID of environment"
}

variable "service_name" {
  type        = string
  description = "Name of service to provision (used as name of SA)"
}
