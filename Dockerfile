# syntax=docker/dockerfile:1

ARG GOLANG_VERSION=1.20
ARG ALPINE_VERSION=3.16


################
# Build application
FROM golang:${GOLANG_VERSION}-alpine${ALPINE_VERSION} AS build

WORKDIR /src

RUN apk update && apk add make git

COPY go.mod go.sum ./
RUN --mount=type=secret,id=netrc,target=/root/.netrc go mod download

COPY . .

# Build binary
RUN make build

################
# Some binary external dependencies
FROM alpine:${ALPINE_VERSION} as deps

WORKDIR /src

ARG WRENCH_VERSION=v1.1.0
ARG LINKERD_AWAIT_VERSION=v0.2.7

ADD https://github.com/cloudspannerecosystem/wrench/releases/download/${WRENCH_VERSION}/wrench_linux_amd64 ./wrench
ADD https://github.com/linkerd/linkerd-await/releases/download/release%2F${LINKERD_AWAIT_VERSION}/linkerd-await-${LINKERD_AWAIT_VERSION}-amd64  ./linkerd-await

RUN chmod +x wrench linkerd-await


################
# Build target image
FROM alpine:${ALPINE_VERSION}

WORKDIR /app

# Copy external binaries
COPY --from=deps /src/wrench .
COPY --from=deps /src/linkerd-await .

# disable linkerd-await by default
ENV LINKERD_AWAIT_DISABLED=1

# Copy configuration
COPY .cfg ./.cfg

# Copy migration binary and migration files to target image
COPY .db/spanner/migrate.sh .
COPY .db/spanner/migrations/ ./migrations/
RUN chmod +x ./migrate.sh

# Copy app binary to target image
COPY --from=build /src/bin/grpcapp .

EXPOSE 8080
EXPOSE 8082
EXPOSE 8084

CMD ["/app/grpcapp"]
