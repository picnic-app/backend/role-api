export GO111MODULE=on
export GOPROXY=https://proxy.golang.org
export GOSUMDB=off

LOCAL_BIN:=$(CURDIR)/bin
BUILD_ENVPARMS:=CGO_ENABLED=0

PLATFORM:=amd64

GIT_TAG:=$(shell git describe --exact-match --abbrev=0 --tags 2> /dev/null)
GIT_HASH:=$(shell git log --format="%h" -n 1 2> /dev/null)
GIT_LOG:=$(shell git log --decorate --oneline -n1 2> /dev/null | base64 | tr -d '\n')
GIT_BRANCH:=$(shell git branch 2> /dev/null | grep '*' | cut -f2 -d' ')
ENV_ID:=$(shell git branch --show-current 2>/dev/null | tr '[:upper:]' '[:lower:]')

APP_VERSION:=$(if $(GIT_BRANCH),$(GIT_BRANCH),$(GIT_HASH))

LDFLAGS:=-X 'platform/core/app.Name=role-api'\
		 -X 'platform/core/app.Version=$(APP_VERSION)'

export PATH:=$(LOCAL_BIN):$(PATH)

MIGRATIONS_DIR:=$(CURDIR)/.db/spanner
SPANNER_DATABASE:=user

GOLANGCI_BIN:=$(LOCAL_BIN)/golangci-lint
GOLANGCI_TAG:=1.50.1

.PHONY: deps
deps:
	$(info Installing binary dependencies...)
	GOBIN=$(LOCAL_BIN) go install github.com/incu6us/goimports-reviser/v3@latest
	GOBIN=$(LOCAL_BIN) go install github.com/fatih/faillint@latest
	GOBIN=$(LOCAL_BIN) go install github.com/cloudspannerecosystem/wrench@latest
	GOBIN=$(LOCAL_BIN) go install mvdan.cc/gofumpt@latest
	npm install cspell --prefix $(LOCAL_BIN)

.PHONY: clean
clean:
	rm -rf .bin

.PHONY: spell
spell:
	$(info Spell checking for the source code...)
	$(LOCAL_BIN)/node_modules/cspell/bin.js "**" --config ./cspell.yml 

.PHONY: build
build:
	$(info Building...)
	$(BUILD_ENVPARMS) go build -ldflags "$(LDFLAGS)" -o ./bin/grpcapp ./cmd

.PHONY: run
run:
	$(info Running...)
	$(BUILD_ENVPARMS) go run -ldflags "$(LDFLAGS)" ./cmd

.PHONY: migrate-new
migrate-new:
	GOBIN=$(LOCAL_BIN) wrench migrate create --directory $(MIGRATIONS_DIR)

.PHONY: migrate-up
migrate-up:
	GOBIN=$(LOCAL_BIN) wrench migrate up --directory $(MIGRATIONS_DIR) \
		--project $(SPANNER_PROJECT) \
		--instance $(SPANNER_INSTANCE) \
		--database $(SPANNER_DATABASE)

.PHONY: migrate-load
migrate-load:
	GOBIN=$(LOCAL_BIN) wrench load --directory $(MIGRATIONS_DIR)/dump \
		--project $(SPANNER_PROJECT) \
		--instance $(SPANNER_INSTANCE) \
		--database $(SPANNER_DATABASE)


.PHONY: format
format:
	goimports-reviser -project-name role-api -company-prefixes gitlab.com/picnic-app ./...
	GOBIN=$(LOCAL_BIN) gofumpt -l -w -extra .

.PHONY: lint
lint:
	go clean --cache
	go mod tidy && go mod vendor
	docker run --rm -v $(CURDIR):/role-api -w /role-api golangci/golangci-lint:v1.48.0-alpine golangci-lint run --config=.cfg/lint.yaml ./... -v
	GOBIN=$(LOCAL_BIN) faillint -paths "errors=github.com/pkg/errors" ./...

.PHONY: docker-image
docker-image:
	$(info Building docker image...)
	docker build --secret "id=netrc,src=$(HOME)/.netrc" --platform $(PLATFORM) --tag role-api -f Dockerfile .

.PHONY: docker-run
docker-run:
	$(info Running docker image...)
	docker run -d -p 8080:8080 -p 8082:8082 -p 8084:8084 role-api

.PHONY: emulator/create
emulator/create:
	docker run -p 9010:9010 -p 9020:9020 -d --name spanner-dev gcr.io/cloud-spanner-emulator/emulator:1.4.8
	gcloud config configurations create emulator --activate --quiet 2> /dev/null ; true
	gcloud config set project dev -q
	gcloud config set api_endpoint_overrides/spanner http://localhost:9020/
	gcloud spanner instances create dev --config=emulator-config --nodes=1 --description="dev instance"
	gcloud spanner databases create dev --instance=dev
	SPANNER_EMULATOR_HOST=localhost:9010 $(MAKE) migrate-up SPANNER_PROJECT=dev SPANNER_INSTANCE=dev SPANNER_DATABASE=dev

.PHONY: emulator/destroy
emulator/destroy:
	docker stop spanner-dev
	docker rm spanner-dev

.PHONY: emulator/migrate
emulator/migrate:
	SPANNER_EMULATOR_HOST=localhost:9010 $(MAKE) migrate-up SPANNER_PROJECT=dev SPANNER_INSTANCE=dev SPANNER_DATABASE=dev

.PHONY: test/unit
test/unit:
	go test -race $(shell go list ./... | grep -v test)

.PHONY: test/e2e
test/e2e:
	go clean -testcache
	go test -race -v --tags=integration -race ./cmd/e2etest

.PHONY: test/all
test/all:
	$(MAKE) test/unit
	$(MAKE) test/e2e

.PHONY: devspace-start
devspace-start:
	$(info Starting devspace)
	DEVSPACE_CONFIG=.cfg/devspace.yaml devspace dev --show-ui -n "$(ENV_ID)"

.PHONY: devspace-stop
devspace-stop:
	$(info Stopping devspace)
	DEVSPACE_CONFIG=.cfg/devspace.yaml devspace purge -n "$(ENV_ID)"
