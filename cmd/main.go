package main

import (
	"context"
	"log"
	"os"
	"syscall"
	"time"

	"gitlab.com/picnic-app/backend/libs/golang/config"
	"gitlab.com/picnic-app/backend/libs/golang/core"
	"gitlab.com/picnic-app/backend/libs/golang/core/mw"
	"gitlab.com/picnic-app/backend/libs/golang/graceful"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
	"gitlab.com/picnic-app/backend/libs/golang/monitoring/monitoring"
	"gitlab.com/picnic-app/backend/libs/golang/monitoring/tracing"
	"gitlab.com/picnic-app/backend/role-api/internal/controller"
	"gitlab.com/picnic-app/backend/role-api/internal/repo/spanner"
	"gitlab.com/picnic-app/backend/role-api/internal/service"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	err := config.Load(ctx)
	if err != nil {
		logger.Fatal(ctx, err)
	}

	logger.InitWithString(config.LogLevel())

	err = tracing.SetupExporter(ctx)
	if err != nil {
		logger.Errorf(ctx, "failed to set up tracing exporter: %v", err)
	}

	monitoring.RegisterPrometheusSuffix()

	app, err := core.InitApp(ctx)
	if err != nil {
		log.Fatal(err)
	}
	app.WithMW(mw.NewServerContextInterceptor(config.String("env.auth.secret")))

	debugSrv, err := app.RunDebug(ctx)
	if err != nil {
		logger.Fatalf(ctx, "failed to start debug server: %v", err)
	}

	ctrl, cleanup, err := initController(ctx, app.Config())
	if err != nil {
		logger.Fatalf(ctx, "failed to init controller: %v", err)
	}
	defer cleanup()

	gracefulShutdown := graceful.New(
		&graceful.ShutdownManagerOptions{Timeout: 60 * time.Second},
		graceful.Parallel(
			&graceful.ParallelShutdownOptions{
				Name:    "servers",
				Timeout: 30 * time.Second,
			},
			graceful.ShutdownErrorFunc(func() error {
				app.Close()
				return nil
			}),
			graceful.HTTPServer(debugSrv),
		),
		graceful.Context(cancel),
		graceful.Parallel(
			&graceful.ParallelShutdownOptions{
				Name:    "clients",
				Timeout: 30 * time.Second,
			},
			graceful.ShutdownErrorFunc(func() error {
				cleanup()
				return nil
			}),
			graceful.Tracer(),
		),
		graceful.Logger(nil),
	)
	gracefulShutdown.RegisterSignals(os.Interrupt, syscall.SIGTERM)
	defer func() {
		_ = gracefulShutdown.Shutdown(context.Background())
	}()

	err = app.Run(ctx, ctrl)
	if err != nil {
		logger.Fatal(ctx, err)
	}

	logger.Info(ctx, "gRPC server closed gracefully")
}

func initController(ctx context.Context, cfg config.Config) (controller.Controller, func(), error) {
	repo, err := spanner.NewRepo(ctx, cfg.Spanner)
	if err != nil {
		return controller.Controller{}, nil, err
	}

	svc := service.New(repo)

	return controller.New(svc), func() {
		repo.Close()
	}, nil
}
