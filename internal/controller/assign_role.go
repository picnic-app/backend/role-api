package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/errors"
)

func (c Controller) AssignRole(ctx context.Context, req *v1.AssignRoleRequest) (*v1.AssignRoleResponse, error) {
	if req == nil {
		return nil, errors.InvalidArgumentError("request")
	}

	err := c.service.AssignRole(ctx, req.EntityId, req.RoleId, req.CircleId, req.UserId)
	if err != nil {
		return nil, err
	}

	return &v1.AssignRoleResponse{}, nil
}
