package controller_test

import (
	"context"
	"reflect"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
)

func TestController_AssignRole_Validation(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	role, err := setUpRole(container)
	require.NoError(t, err)

	for _, test := range []struct {
		name string
		ctx  context.Context
		req  *v1.AssignRoleRequest
		want codes.Code
	}{
		{
			name: "no request",
			ctx:  context.Background(),
			req:  nil,
			want: codes.InvalidArgument,
		},
		{
			name: "no entity id",
			ctx:  context.Background(),
			req: &v1.AssignRoleRequest{
				RoleId: role.Id,
			},
			want: codes.InvalidArgument,
		},
		{
			name: "no role id",
			ctx:  context.Background(),
			req: &v1.AssignRoleRequest{
				EntityId: uuid.NewString(),
			},
			want: codes.InvalidArgument,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.AssignRole(test.ctx, test.req)
			got := status.Code(err)
			require.Equal(t, test.want, got, err)
		})
	}
}

func TestController_AssignRole(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	role, err := setUpRole(container)
	require.NoError(t, err)

	entityID := uuid.NewString()
	circleID := uuid.NewString()
	userID := uuid.NewString()

	ctx := context.Background()

	// assign
	_, err = container.controller.AssignRole(
		ctx,
		&v1.AssignRoleRequest{
			EntityId: entityID,
			RoleId:   role.Id,
			CircleId: circleID,
			UserId:   userID,
		},
	)
	require.NoError(t, err)

	// get assigned roles
	getResponse, err := container.controller.GetAssignedRoles(
		ctx,
		&v1.GetAssignedRolesRequest{
			EntityId: entityID,
			CircleId: circleID,
			UserId:   userID,
		},
	)
	require.NoError(t, err)

	require.NotNil(t, getResponse.Scopes)
	require.Len(t, getResponse.Scopes, 1)
	scope := getResponse.Scopes[0]
	require.Equal(t, scope.RoleId, role.Id)
	require.Equal(t, scope.CircleId, circleID)
	require.Equal(t, scope.UserId, userID)

	require.NotNil(t, getResponse.Roles)
	require.Len(t, getResponse.Roles, 1)
	assigned := getResponse.Roles[0]
	require.True(t, reflect.DeepEqual(role, assigned))
}
