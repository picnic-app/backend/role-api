package controller_test

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/model"
)

func TestController_Authorize_Authentication(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	appID := uuid.NewString()
	circleID := uuid.NewString()

	role, err := setUpAssignedRole(container, appID, circleID, "")
	require.NoError(t, err)
	permission := role.Permissions[0]

	request := &v1.AuthorizeRequest{
		Service:   permission.Service,
		Operation: permission.Operation,
		CircleIds: []string{circleID},
	}

	for _, test := range []struct {
		name    string
		ctx     context.Context
		request *v1.AuthorizeRequest
		want    codes.Code
	}{
		{
			name:    "returns Unauthenticated error if appID and userID are empty",
			ctx:     context.Background(),
			request: request,
			want:    codes.Unauthenticated,
		},
		{
			name:    "returns Unimplemented error if userID is not empty",
			ctx:     auth.AddUserIDToCtx(context.Background(), uuid.NewString()),
			request: request,
			want:    codes.Unimplemented,
		},
		{
			name:    "succeeds with app context",
			ctx:     auth.AddAppIDToCtx(context.Background(), appID),
			request: request,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.Authorize(test.ctx, test.request)
			if test.want != 0 {
				got := status.Code(err)
				require.Equal(t, test.want, got, err)
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestController_Authorize_Validation(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	appID := uuid.NewString()
	circleID := uuid.NewString()

	role, err := setUpAssignedRole(container, appID, circleID, "")
	require.NoError(t, err)
	permission := role.Permissions[0]

	ctx := auth.AddAppIDToCtx(context.Background(), appID)

	for _, test := range []struct {
		name string
		req  *v1.AuthorizeRequest
		want codes.Code
	}{
		{
			name: "returns InvalidArgument if request is nil",
			req:  nil,
			want: codes.InvalidArgument,
		},
		{
			name: "returns InvalidArgument if service is empty",
			req: &v1.AuthorizeRequest{
				Operation: permission.Operation,
				CircleIds: []string{circleID},
			},
			want: codes.InvalidArgument,
		},
		{
			name: "returns InvalidArgument if operation is empty",
			req: &v1.AuthorizeRequest{
				Service:   permission.Service,
				CircleIds: []string{circleID},
			},
			want: codes.InvalidArgument,
		},
		{
			name: "returns InvalidArgument if circleIDs is empty",
			req: &v1.AuthorizeRequest{
				Service:   permission.Service,
				Operation: permission.Operation,
			},
			want: codes.InvalidArgument,
		},
		{
			name: "returns Unimplemented if userIDs is not empty",
			req: &v1.AuthorizeRequest{
				Service:   permission.Service,
				Operation: permission.Operation,
				UserIds:   []string{uuid.NewString()},
			},
			want: codes.Unimplemented,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.Authorize(ctx, test.req)
			got := status.Code(err)
			require.Equal(t, test.want, got, err)
		})
	}
}

func TestController_Authorize(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	appID := uuid.NewString()
	circle1ID := uuid.NewString()
	circle2ID := uuid.NewString()

	sharedPermission := model.Permission{
		Service:   "Content",
		Operation: "CreatePost",
		Scope:     "Kind = Image",
	}

	_, err := setUpAssignedRole(
		container,
		appID,
		circle1ID,
		"",
		sharedPermission,
		model.Permission{
			Service:   "Content",
			Operation: "CreateComment",
			Scope:     "*",
		})
	require.NoError(t, err)

	_, err = setUpAssignedRole(
		container,
		appID,
		circle2ID,
		"",
		sharedPermission,
		model.Permission{
			Service:   "Chat",
			Operation: "SendMessage",
			Scope:     "*",
		})
	require.NoError(t, err)

	ctx := auth.AddAppIDToCtx(context.Background(), appID)

	t.Run("single circle ID", func(t *testing.T) {
		circleIDs := []string{circle1ID}
		got, err := container.controller.Authorize(ctx, &v1.AuthorizeRequest{
			Service:   sharedPermission.Service,
			Operation: sharedPermission.Operation,
			CircleIds: circleIDs,
		})
		require.NoError(t, err)
		require.NotNil(t, got)
		require.Len(t, got.Scopes, len(circleIDs))

		for _, scope := range got.Scopes {
			require.Equal(t, scope, sharedPermission.Scope)
		}
	})

	t.Run("multiple circle ID", func(t *testing.T) {
		circleIDs := []string{circle1ID, circle2ID}
		got, err := container.controller.Authorize(ctx, &v1.AuthorizeRequest{
			Service:   sharedPermission.Service,
			Operation: sharedPermission.Operation,
			CircleIds: circleIDs,
		})
		require.NoError(t, err)
		require.NotNil(t, got)
		require.Len(t, got.Scopes, len(circleIDs))

		for _, scope := range got.Scopes {
			require.Equal(t, scope, sharedPermission.Scope)
		}
	})
}
