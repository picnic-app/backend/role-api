package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/errors"
)

func (c Controller) Authorize(ctx context.Context, req *v1.AuthorizeRequest) (*v1.AuthorizeResponse, error) {
	if req == nil {
		return nil, errors.InvalidArgumentError("request")
	}

	scopes, err := c.service.Authorize(ctx, req.Service, req.Operation, req.CircleIds, req.UserIds)
	if err != nil {
		return nil, err
	}

	return &v1.AuthorizeResponse{
		Scopes: scopes,
	}, nil
}
