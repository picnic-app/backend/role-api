package controller_test

import (
	"context"

	"github.com/google/uuid"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/controller/serialize"
	"gitlab.com/picnic-app/backend/role-api/internal/model"
)

func setUpRole(container Container) (*v1.Role, error) {
	name := uuid.NewString()
	permissions := []*v1.Permission{
		{
			Service:   "Content",
			Operation: "CreatePost",
			Scope:     "*",
		},
		{
			Service:   "Chat",
			Operation: "SendMessage",
			Scope:     "*",
		},
	}

	ctx := context.Background()

	// create
	createResponse, err := container.controller.CreateRole(
		ctx,
		&v1.CreateRoleRequest{
			Role: &v1.Role{
				Name:        name,
				Permissions: permissions,
			},
		},
	)
	if err != nil {
		return nil, err
	}

	// get
	getResponse, err := container.controller.GetRole(
		ctx,
		&v1.GetRoleRequest{
			Id: createResponse.Id,
		},
	)
	if err != nil {
		return nil, err
	}

	return getResponse.Role, nil
}

func setUpAssignedRole(
	container Container,
	entityID, circleID, userID string,
	permissions ...model.Permission) (*v1.Role, error) {
	name := uuid.NewString()

	if len(permissions) == 0 {
		permissions = append(permissions,
			model.Permission{
				Service:   "Content",
				Operation: "CreatePost",
				Scope:     "*",
			},
			model.Permission{
				Service:   "Chat",
				Operation: "SendMessage",
				Scope:     "*",
			},
		)
	}

	ctx := context.Background()

	// create
	createResponse, err := container.controller.CreateRole(
		ctx,
		&v1.CreateRoleRequest{
			Role: &v1.Role{
				Name:        name,
				Permissions: serialize.Permissions(permissions),
			},
		},
	)
	if err != nil {
		return nil, err
	}

	// get
	getResponse, err := container.controller.GetRole(
		ctx,
		&v1.GetRoleRequest{
			Id: createResponse.Id,
		},
	)
	if err != nil {
		return nil, err
	}

	// assign
	_, err = container.controller.AssignRole(
		ctx,
		&v1.AssignRoleRequest{
			EntityId: entityID,
			RoleId:   getResponse.Role.Id,
			CircleId: circleID,
			UserId:   userID,
		},
	)
	if err != nil {
		return nil, err
	}

	return getResponse.Role, nil
}
