package controller

import (
	"google.golang.org/grpc"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/service"
)

func New(svc *service.Service) Controller {
	return Controller{
		service: svc,
	}
}

// Controller represents struct that implements RoleServiceServer.
type Controller struct {
	service *service.Service

	v1.UnsafeRoleAPIServer
}

func (c Controller) Register(reg grpc.ServiceRegistrar) {
	v1.RegisterRoleAPIServer(reg, c)
}
