package controller_test

import (
	"context"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/libs/golang/config"
	"gitlab.com/picnic-app/backend/role-api/internal/controller"
	spannerRepo "gitlab.com/picnic-app/backend/role-api/internal/repo/spanner"
	"gitlab.com/picnic-app/backend/role-api/internal/service"
)

var (
	spannerCli *spanner.Client
	httpServer *httptest.Server
	mux        = &http.ServeMux{}
)

type Container struct {
	mux        *http.ServeMux
	spannerCli *spanner.Client
	service    *service.Service
	controller controller.Controller
	repo       spannerRepo.Repo
}

func initContainer(t *testing.T) Container {
	repo := spannerRepo.NewRepoWithClient(spannerCli)
	svc := service.New(repo)

	return Container{
		mux:        mux,
		service:    svc,
		repo:       repo,
		spannerCli: spannerCli,
		controller: controller.New(svc),
	}
}

func TestMain(m *testing.M) {
	if isCI() {
		return
	}

	// defer goleak.VerifyTestMain(m)

	// Change the working dir to the root dir
	if err := os.Chdir("./../.."); err != nil {
		log.Fatal(err)
	}

	ctx := context.Background()

	if err := config.Load(ctx); err != nil {
		log.Fatal("failed to load config: ", err)
	}

	cfg, err := config.GetConfig(ctx)
	if err != nil {
		log.Fatal("failed to load config: ", err)
	}

	spannerCli, err = spannerRepo.NewSpannerClient(ctx, cfg.Spanner.DSN, cfg.Spanner.ADCPath)
	if err != nil {
		log.Fatal("failed to create spanner client: ", err)
	}

	row := spannerCli.Single().Query(ctx, spanner.Statement{SQL: "SELECT 1;"})
	err = row.Do(func(r *spanner.Row) error { return nil })
	if err != nil {
		log.Fatal("failed to execute query with spanner client: ", err)
	}

	httpServer = httptest.NewServer(mux)
	defer httpServer.Close()

	code := m.Run()

	spannerCli.Close()

	os.Exit(code)
}

// isCI checks if the test is running on a CI environment.
func isCI() bool {
	value, found := os.LookupEnv("CI")
	if !found {
		return false
	}
	return strings.ToLower(value) != "false"
}
