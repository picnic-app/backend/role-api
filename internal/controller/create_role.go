package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/controller/deserialize"
	"gitlab.com/picnic-app/backend/role-api/internal/errors"
)

func (c Controller) CreateRole(ctx context.Context, req *v1.CreateRoleRequest) (*v1.CreateRoleResponse, error) {
	if req == nil {
		return nil, errors.InvalidArgumentError("request")
	}

	if req.Role == nil {
		return nil, errors.InvalidArgumentError("role")
	}

	role := deserialize.Role(req.Role)

	out, err := c.service.CreateRole(ctx, role)
	if err != nil {
		return nil, err
	}

	return &v1.CreateRoleResponse{
		Id: out.ID,
	}, nil
}
