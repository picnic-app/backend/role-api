package controller_test

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
)

func TestController_CreateRole_Validation(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	for _, test := range []struct {
		name string
		ctx  context.Context
		req  *v1.CreateRoleRequest
		want codes.Code
	}{
		{
			name: "no request",
			ctx:  context.Background(),
			req:  nil,
			want: codes.InvalidArgument,
		},
		{
			name: "no role",
			ctx:  context.Background(),
			req:  &v1.CreateRoleRequest{},
			want: codes.InvalidArgument,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.CreateRole(test.ctx, test.req)
			got := status.Code(err)
			require.Equal(t, test.want, got, err)
		})
	}
}

func TestController_CreateRole(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	name := uuid.NewString()
	permissions := []*v1.Permission{
		{
			Service:   "Content",
			Operation: "CreatePost",
			Scope:     "*",
		},
		{
			Service:   "Chat",
			Operation: "SendMessage",
			Scope:     "*",
		},
	}

	ctx := context.Background()
	createResponse, err := container.controller.CreateRole(
		ctx,
		&v1.CreateRoleRequest{
			Role: &v1.Role{
				Name:        name,
				Permissions: permissions,
			},
		},
	)
	require.NoError(t, err)
	require.NotEmpty(t, createResponse.Id)

	id := createResponse.Id

	// get role
	getResponse, err := container.controller.GetRole(
		ctx,
		&v1.GetRoleRequest{
			Id: id,
		},
	)
	require.NoError(t, err)
	require.NotNil(t, getResponse.Role)

	role := getResponse.Role

	require.Equal(t, id, role.Id)
	require.Equal(t, name, role.Name)
	require.Equal(t, permissions, role.Permissions)
}
