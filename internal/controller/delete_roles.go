package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/errors"
)

func (c Controller) DeleteRoles(ctx context.Context, req *v1.DeleteRolesRequest) (*v1.DeleteRolesResponse, error) {
	if req == nil {
		return nil, errors.InvalidArgumentError("request")
	}

	err := c.service.DeleteRoles(ctx, req.GetIds()...)
	if err != nil {
		return nil, err
	}

	return &v1.DeleteRolesResponse{}, nil
}
