package controller_test

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/errors"
)

func TestController_DeleteRoles_Validation(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	for _, test := range []struct {
		name string
		ctx  context.Context
		req  *v1.DeleteRolesRequest
		want codes.Code
	}{
		{
			name: "no request",
			ctx:  context.Background(),
			req:  nil,
			want: codes.InvalidArgument,
		},
		{
			name: "no role IDs",
			ctx:  context.Background(),
			req:  &v1.DeleteRolesRequest{},
			want: codes.InvalidArgument,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.DeleteRoles(test.ctx, test.req)
			got := status.Code(err)
			require.Equal(t, test.want, got, err)
		})
	}
}

func TestController_DeleteRoles(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	entityID := uuid.NewString()
	circleID := uuid.NewString()
	userID := uuid.NewString()

	role, err := setUpAssignedRole(container, entityID, circleID, userID)
	require.NoError(t, err)

	ctx := context.Background()

	// delete role
	deleteResponse, err := container.controller.DeleteRoles(
		ctx,
		&v1.DeleteRolesRequest{
			Ids: []string{role.Id},
		},
	)
	require.NoError(t, err)
	require.NotNil(t, deleteResponse)

	// get role
	getRoleResponse, err := container.controller.GetRole(
		ctx,
		&v1.GetRoleRequest{
			Id: role.Id,
		},
	)
	require.Error(t, err)
	require.Equal(t, codes.NotFound, errors.GetCode(err))
	require.Nil(t, getRoleResponse)

	// get entity assigned roles
	getAssignedRolesResponse, err := container.controller.GetAssignedRoles(
		ctx,
		&v1.GetAssignedRolesRequest{
			EntityId: entityID,
		},
	)
	require.NoError(t, err)
	require.Len(t, getAssignedRolesResponse.Roles, 0)
}
