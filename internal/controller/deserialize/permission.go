package deserialize

import (
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/model"
)

func Permission(in *v1.Permission) model.Permission {
	return model.Permission{Service: in.Service, Operation: in.Operation, Scope: in.Scope}
}

func Permissions(in []*v1.Permission) []model.Permission {
	result := make([]model.Permission, len(in))
	for i, p := range in {
		result[i] = Permission(p)
	}
	return result
}
