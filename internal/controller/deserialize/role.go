package deserialize

import (
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/model"
)

func Role(in *v1.Role) model.Role {
	return model.Role{ID: in.Id, Name: in.Name, Permissions: Permissions(in.Permissions)}
}

func Roles(in []*v1.Role) []model.Role {
	out := make([]model.Role, len(in))
	for i, r := range in {
		out[i] = Role(r)
	}
	return out
}
