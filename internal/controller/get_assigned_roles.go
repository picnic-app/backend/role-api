package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/controller/serialize"
	"gitlab.com/picnic-app/backend/role-api/internal/errors"
	"gitlab.com/picnic-app/backend/role-api/internal/model"
)

func (c Controller) GetAssignedRoles(ctx context.Context, req *v1.GetAssignedRolesRequest) (
	*v1.GetAssignedRolesResponse, error,
) {
	if req == nil {
		return nil, errors.InvalidArgumentError("request")
	}

	assignedRoles, err := c.service.GetAssignedRoles(ctx, req.EntityId, req.CircleId, req.UserId)
	if err != nil {
		return nil, err
	}

	if len(assignedRoles) == 0 {
		return &v1.GetAssignedRolesResponse{
			Scopes: []*v1.Scope{},
			Roles:  []*v1.Role{},
		}, nil
	}

	var roleIDs []string
	unique := map[string]struct{}{}
	for _, assignedRole := range assignedRoles {
		_, ok := unique[assignedRole.RoleID]
		if ok {
			continue
		}

		unique[assignedRole.RoleID] = struct{}{}
		roleIDs = append(roleIDs, assignedRole.RoleID)
	}

	roles, err := c.service.GetRoles(ctx, roleIDs)
	if err != nil {
		return nil, err
	}

	out := make([]model.Role, 0, len(roles))
	for _, role := range roles {
		if role.DeletedAt != nil {
			continue
		}
		out = append(out, role)
	}

	return &v1.GetAssignedRolesResponse{
		Roles:  serialize.Roles(out),
		Scopes: serialize.Scopes(assignedRoles),
	}, nil
}
