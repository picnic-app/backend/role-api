package controller_test

import (
	"context"
	"reflect"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
)

func TestController_GetAssignedRoles_Validation(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	for _, test := range []struct {
		name string
		ctx  context.Context
		req  *v1.GetAssignedRolesRequest
		want codes.Code
	}{
		{
			name: "no request",
			ctx:  context.Background(),
			req:  nil,
			want: codes.InvalidArgument,
		},
		{
			name: "no entity id",
			ctx:  context.Background(),
			req:  &v1.GetAssignedRolesRequest{},
			want: codes.InvalidArgument,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.GetAssignedRoles(test.ctx, test.req)
			got := status.Code(err)
			require.Equal(t, test.want, got, err)
		})
	}
}

func TestController_GetAssignedRoles(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	role1, err := setUpRole(container)
	require.NoError(t, err)

	_, err = setUpRole(container)
	require.NoError(t, err)

	entities := []string{uuid.NewString(), uuid.NewString()}
	circles := []string{uuid.NewString(), uuid.NewString()}
	users := []string{uuid.NewString(), uuid.NewString()}

	ctx := context.Background()

	for _, entityID := range entities {
		// no scope
		_, err = container.controller.AssignRole(
			ctx,
			&v1.AssignRoleRequest{
				EntityId: entityID,
				RoleId:   role1.Id,
			},
		)
		require.NoError(t, err)

		// circle scope only
		for _, circleID := range circles {
			_, err = container.controller.AssignRole(
				ctx,
				&v1.AssignRoleRequest{
					EntityId: entityID,
					RoleId:   role1.Id,
					CircleId: circleID,
				},
			)
			require.NoError(t, err)
		}

		// user scope only
		for _, userID := range users {
			_, err = container.controller.AssignRole(
				ctx,
				&v1.AssignRoleRequest{
					EntityId: entityID,
					RoleId:   role1.Id,
					UserId:   userID,
				},
			)
			require.NoError(t, err)
		}
	}

	for _, entityID := range entities {
		// no scope
		getResponse, err := container.controller.GetAssignedRoles(
			ctx,
			&v1.GetAssignedRolesRequest{
				EntityId: entityID,
			},
		)
		require.NoError(t, err)
		require.NotNil(t, getResponse)

		require.Len(t, getResponse.Roles, 1)
		for _, role := range getResponse.Roles {
			require.True(t, reflect.DeepEqual(role1, role))
		}

		require.Len(t, getResponse.Scopes, 1+len(users)+len(circles))
		for _, scope := range getResponse.Scopes {
			require.Equal(t, role1.Id, scope.RoleId)
		}

		// get roles with circle scope
		for _, circleID := range circles {
			getResponse, err = container.controller.GetAssignedRoles(
				ctx,
				&v1.GetAssignedRolesRequest{
					EntityId: entityID,
					CircleId: circleID,
				},
			)
			require.NoError(t, err)
			require.NotNil(t, getResponse)

			require.Len(t, getResponse.Roles, 1)
			for _, role := range getResponse.Roles {
				require.True(t, reflect.DeepEqual(role1, role))
			}

			require.Len(t, getResponse.Scopes, 1)
			for _, scope := range getResponse.Scopes {
				require.Equal(t, role1.Id, scope.RoleId)
				require.Equal(t, circleID, scope.CircleId)
			}
		}

		// get roles with user scope
		for _, userID := range users {
			getResponse, err = container.controller.GetAssignedRoles(
				ctx,
				&v1.GetAssignedRolesRequest{
					EntityId: entityID,
					UserId:   userID,
				},
			)
			require.NoError(t, err)
			require.NotNil(t, getResponse)

			require.Len(t, getResponse.Roles, 1)
			for _, role := range getResponse.Roles {
				require.True(t, reflect.DeepEqual(role1, role))
			}

			require.Len(t, getResponse.Scopes, 1)
			for _, scope := range getResponse.Scopes {
				require.Equal(t, role1.Id, scope.RoleId)
				require.Equal(t, userID, scope.UserId)
			}
		}
	}
}
