package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/controller/serialize"
	"gitlab.com/picnic-app/backend/role-api/internal/errors"
)

func (c Controller) GetRole(ctx context.Context, req *v1.GetRoleRequest) (*v1.GetRoleResponse, error) {
	if req == nil {
		return nil, errors.InvalidArgumentError("request")
	}

	role, err := c.service.GetRole(ctx, req.GetId())
	if err != nil {
		return nil, err
	}

	return &v1.GetRoleResponse{
		Role: serialize.Role(role),
	}, nil
}
