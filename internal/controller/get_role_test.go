package controller_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
)

func TestController_GetRole_Validation(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	for _, test := range []struct {
		name string
		ctx  context.Context
		req  *v1.GetRoleRequest
		want codes.Code
	}{
		{
			name: "no request",
			ctx:  context.Background(),
			req:  nil,
			want: codes.InvalidArgument,
		},
		{
			name: "no role ID",
			ctx:  context.Background(),
			req:  &v1.GetRoleRequest{},
			want: codes.InvalidArgument,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.GetRole(test.ctx, test.req)
			got := status.Code(err)
			require.Equal(t, test.want, got, err)
		})
	}
}

func TestController_GetRole(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	role, err := setUpRole(container)
	require.NoError(t, err)

	ctx := context.Background()

	// get role
	getResponse, err := container.controller.GetRole(
		ctx,
		&v1.GetRoleRequest{
			Id: role.Id,
		},
	)
	require.NoError(t, err)
	require.NotNil(t, getResponse.Role)

	got := getResponse.Role

	require.Equal(t, role.Id, got.Id)
	require.Equal(t, role.Name, got.Name)
	require.Len(t, got.Permissions, len(role.Permissions))

	for _, wantPermission := range role.Permissions {
		var found *v1.Permission
		for _, gotPermission := range role.Permissions {
			if gotPermission.Service != wantPermission.Service {
				continue
			}
			found = gotPermission
			break
		}
		require.NotNil(t, found)
		require.Equal(t, wantPermission, found)
	}
}
