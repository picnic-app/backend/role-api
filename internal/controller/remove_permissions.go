package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/controller/deserialize"
	"gitlab.com/picnic-app/backend/role-api/internal/errors"
)

func (c Controller) RemovePermissions(ctx context.Context, req *v1.RemovePermissionsRequest) (
	*v1.RemovePermissionsResponse, error,
) {
	if req == nil {
		return nil, errors.InvalidArgumentError("request")
	}

	err := c.service.RemovePermissions(ctx, req.RoleId, deserialize.Permissions(req.Permissions))
	if err != nil {
		return nil, err
	}

	return &v1.RemovePermissionsResponse{}, nil
}
