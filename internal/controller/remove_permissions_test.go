package controller_test

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/errors"
)

func TestController_RemovePermissions_Validation(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	for _, test := range []struct {
		name string
		ctx  context.Context
		req  *v1.RemovePermissionsRequest
		want codes.Code
	}{
		{
			name: "no request",
			ctx:  context.Background(),
			req:  nil,
			want: codes.InvalidArgument,
		},
		{
			name: "no role ID",
			ctx:  context.Background(),
			req:  &v1.RemovePermissionsRequest{},
			want: codes.InvalidArgument,
		},
		{
			name: "no permission",
			ctx:  context.Background(),
			req: &v1.RemovePermissionsRequest{
				RoleId: uuid.NewString(),
			},
			want: codes.InvalidArgument,
		},
		{
			name: "role does not exist",
			ctx:  context.Background(),
			req: &v1.RemovePermissionsRequest{
				RoleId: uuid.NewString(),
				Permissions: []*v1.Permission{
					{Service: "Content", Operation: "CreatePost"},
				},
			},
			want: codes.NotFound,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.RemovePermissions(test.ctx, test.req)
			got := errors.GetCode(err)
			require.Equal(t, test.want, got, err)
		})
	}
}

func TestController_RemovePermissions(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	role, err := setUpRole(container)
	require.NoError(t, err)

	ctx := context.Background()

	// remove permissions
	removed := role.Permissions[0:1]
	removeResponse, err := container.controller.RemovePermissions(
		ctx,
		&v1.RemovePermissionsRequest{
			RoleId:      role.Id,
			Permissions: removed,
		},
	)
	require.NoError(t, err)
	require.NotNil(t, removeResponse)

	// get role
	getRoleResponse, err := container.controller.GetRole(
		ctx,
		&v1.GetRoleRequest{
			Id: role.Id,
		},
	)
	require.NoError(t, err)
	role = getRoleResponse.Role

	for _, permission := range removed {
		found := false
		for _, got := range role.Permissions {
			if got.Service != permission.Service || got.Operation != permission.Operation {
				continue
			}
			found = true
		}
		require.False(t, found)
	}
}
