package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/errors"
)

func (c Controller) RevokeRole(ctx context.Context, req *v1.RevokeRoleRequest) (*v1.RevokeRoleResponse, error) {
	if req == nil {
		return nil, errors.InvalidArgumentError("request")
	}

	err := c.service.RevokeRole(ctx, req.EntityId, req.RoleId, req.CircleId, req.UserId)
	if err != nil {
		return nil, err
	}

	return &v1.RevokeRoleResponse{}, nil
}
