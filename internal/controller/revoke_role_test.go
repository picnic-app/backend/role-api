package controller_test

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
)

func TestController_RevokeRole_Validation(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	role, err := setUpRole(container)
	require.NoError(t, err)

	for _, test := range []struct {
		name string
		ctx  context.Context
		req  *v1.RevokeRoleRequest
		want codes.Code
	}{
		{
			name: "no request",
			ctx:  context.Background(),
			req:  nil,
			want: codes.InvalidArgument,
		},
		{
			name: "no entity id",
			ctx:  context.Background(),
			req: &v1.RevokeRoleRequest{
				RoleId: role.Id,
			},
			want: codes.InvalidArgument,
		},
		{
			name: "no role id",
			ctx:  context.Background(),
			req: &v1.RevokeRoleRequest{
				EntityId: uuid.NewString(),
			},
			want: codes.InvalidArgument,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.RevokeRole(test.ctx, test.req)
			got := status.Code(err)
			require.Equal(t, test.want, got, err)
		})
	}
}

func TestController_RevokeRole(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	entityID := uuid.NewString()
	circleID := uuid.NewString()
	userID := uuid.NewString()
	role, err := setUpAssignedRole(container, entityID, circleID, userID)
	require.NoError(t, err)

	ctx := context.Background()

	// revoke
	_, err = container.controller.RevokeRole(
		ctx,
		&v1.RevokeRoleRequest{
			EntityId: entityID,
			RoleId:   role.Id,
		},
	)
	require.NoError(t, err)

	// get assigned roles
	getResponse, err := container.controller.GetAssignedRoles(
		ctx,
		&v1.GetAssignedRolesRequest{
			EntityId: entityID,
			CircleId: circleID,
			UserId:   userID,
		},
	)
	require.NoError(t, err)
	require.NotNil(t, getResponse.Roles)
	require.Len(t, getResponse.Roles, 0)
}
