package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/errors"
)

func (c Controller) RevokeRoles(ctx context.Context, req *v1.RevokeRolesRequest) (*v1.RevokeRolesResponse, error) {
	if req == nil {
		return nil, errors.InvalidArgumentError("request")
	}

	err := c.service.RevokeRoles(ctx, req.EntityId, req.CircleId, req.UserId)
	if err != nil {
		return nil, err
	}

	return &v1.RevokeRolesResponse{}, nil
}
