package controller_test

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
)

func TestController_RevokeRoles_Validation(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	for _, test := range []struct {
		name string
		ctx  context.Context
		req  *v1.RevokeRolesRequest
		want codes.Code
	}{
		{
			name: "no request",
			ctx:  context.Background(),
			req:  nil,
			want: codes.InvalidArgument,
		},
		{
			name: "no entity id",
			ctx:  context.Background(),
			req:  &v1.RevokeRolesRequest{},
			want: codes.InvalidArgument,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.RevokeRoles(test.ctx, test.req)
			got := status.Code(err)
			require.Equal(t, test.want, got, err)
		})
	}
}

func TestController_RevokeRoles(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	entityID := uuid.NewString()
	circle1ID, circle2ID := uuid.NewString(), uuid.NewString()
	userID := uuid.NewString()

	_, err := setUpAssignedRole(container, entityID, circle1ID, userID)
	require.NoError(t, err)
	_, err = setUpAssignedRole(container, entityID, circle1ID, userID)
	require.NoError(t, err)
	role, err := setUpAssignedRole(container, entityID, circle2ID, userID)
	require.NoError(t, err)

	ctx := context.Background()

	// revoke
	_, err = container.controller.RevokeRoles(
		ctx,
		&v1.RevokeRolesRequest{
			EntityId: entityID,
			CircleId: circle1ID,
		},
	)
	require.NoError(t, err)

	// get assigned roles
	getResponse, err := container.controller.GetAssignedRoles(
		ctx,
		&v1.GetAssignedRolesRequest{
			EntityId: entityID,
		},
	)
	require.NoError(t, err)
	require.NotNil(t, getResponse.Roles)
	require.Len(t, getResponse.Roles, 1)
	require.Equal(t, getResponse.Roles[0].Id, role.Id)
}
