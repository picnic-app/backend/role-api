package serialize

import (
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/model"
)

func Permission(in model.Permission) *v1.Permission {
	return &v1.Permission{Service: in.Service, Operation: in.Operation, Scope: in.Scope}
}

func Permissions(in []model.Permission) []*v1.Permission {
	out := make([]*v1.Permission, len(in))
	for i, p := range in {
		out[i] = Permission(p)
	}
	return out
}
