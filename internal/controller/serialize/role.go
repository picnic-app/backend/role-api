package serialize

import (
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/model"
)

func Role(in model.Role) *v1.Role {
	return &v1.Role{Id: in.ID, Name: in.Name, Permissions: Permissions(in.Permissions)}
}

func Roles(in []model.Role) []*v1.Role {
	result := make([]*v1.Role, len(in))
	for i, r := range in {
		result[i] = Role(r)
	}
	return result
}
