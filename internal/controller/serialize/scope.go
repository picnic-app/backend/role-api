package serialize

import (
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
	"gitlab.com/picnic-app/backend/role-api/internal/model"
)

func Scope(in model.AssignedRole) *v1.Scope {
	return &v1.Scope{RoleId: in.RoleID, CircleId: in.CircleID, UserId: in.UserID}
}

func Scopes(in []model.AssignedRole) []*v1.Scope {
	out := make([]*v1.Scope, len(in))
	for i, in := range in {
		out[i] = Scope(in)
	}
	return out
}
