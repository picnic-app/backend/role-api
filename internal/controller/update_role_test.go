package controller_test

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/role-api/role/v1"
)

func TestController_UpdateRole_Validation(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	for _, test := range []struct {
		name string
		ctx  context.Context
		req  *v1.UpdateRoleRequest
		want codes.Code
	}{
		{
			name: "no request",
			ctx:  context.Background(),
			req:  nil,
			want: codes.InvalidArgument,
		},
		{
			name: "no role",
			ctx:  context.Background(),
			req:  &v1.UpdateRoleRequest{},
			want: codes.InvalidArgument,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.UpdateRole(test.ctx, test.req)
			got := status.Code(err)
			require.Equal(t, test.want, got, err)
		})
	}
}

func TestController_UpdateRole(t *testing.T) {
	t.Parallel()

	container := initContainer(t)

	role, err := setUpRole(container)
	require.NoError(t, err)

	name := uuid.NewString()
	permissions := role.Permissions[0:1]

	ctx := context.Background()
	updateResponse, err := container.controller.UpdateRole(
		ctx,
		&v1.UpdateRoleRequest{
			Role: &v1.Role{
				Id:          role.Id,
				Name:        name,
				Permissions: permissions,
			},
		},
	)
	require.NoError(t, err)
	require.NotEmpty(t, updateResponse.Id)

	// get role
	getResponse, err := container.controller.GetRole(
		ctx,
		&v1.GetRoleRequest{
			Id: role.Id,
		},
	)
	require.NoError(t, err)
	require.NotNil(t, getResponse.Role)

	got := getResponse.Role

	require.Equal(t, role.Id, got.Id)
	require.Equal(t, name, got.Name)
	require.Equal(t, permissions, got.Permissions)
}
