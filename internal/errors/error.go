package errors

import (
	"fmt"
	"io"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func New(code codes.Code, message string) error {
	return Error{
		err:   status.Error(code, message),
		stack: callers(),
	}
}

func NewWithError(code codes.Code, message string, inner error) error {
	out := Error{
		err:   status.Error(code, message),
		inner: inner,
	}

	if _, ok := inner.(Error); !ok {
		out.stack = callers()
	}

	return out
}

type Error struct {
	err   error
	inner error
	*stack
}

func (e Error) Error() string {
	return e.err.Error()
}

func (e Error) Format(s fmt.State, verb rune) {
	switch verb {
	case 'v':
		if s.Flag('+') {
			fmt.Fprintf(s, "%s", e.err.Error())
			if e.inner != nil {
				if inner, ok := e.inner.(Error); ok {
					fmt.Fprintf(s, "\ninner error: %+v", inner)
					return
				}
				fmt.Fprintf(s, "\ninner error: %s", e.inner)
			}
			if e.stack != nil {
				e.stack.Format(s, verb)
			}
			return
		}
		fallthrough
	case 's':
		_, _ = io.WriteString(s, e.Error())
	case 'q':
		fmt.Fprintf(s, "%q", e.Error())
	}
}
