package errors

import (
	pkgerrors "github.com/pkg/errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func InvalidArgumentError(param string) error {
	return status.Error(codes.InvalidArgument, param)
}

func NotFoundError(param string) error {
	return status.Error(codes.NotFound, param)
}

func InternalError(err error) error {
	return status.Error(codes.Internal, err.Error())
}

func GetCode(err error) codes.Code {
	var s interface{ GRPCStatus() *status.Status }
	if pkgerrors.As(err, &s) {
		return s.GRPCStatus().Code()
	}

	if target, ok := err.(Error); ok {
		if pkgerrors.As(target.err, &s) {
			return s.GRPCStatus().Code()
		}
	}

	return codes.Unknown
}
