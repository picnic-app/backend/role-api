package model

import (
	"time"
)

type Role struct {
	ID          string
	Name        string
	Permissions []Permission
	CreatedAt   time.Time
	UpdatedAt   *time.Time
	DeletedAt   *time.Time
}

type Permission struct {
	Service   string
	Operation string
	Scope     string
}

type AssignedRole struct {
	ID        string
	EntityID  string
	RoleID    string
	CircleID  string
	UserID    string
	CreatedAt time.Time
}

type RoleUpdate struct {
	Name        *Field
	Permissions *Field
	DeletedAt   *Field
}

type Field struct {
	Value any
}
