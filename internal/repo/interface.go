package repo

import (
	"context"
	"time"

	"gitlab.com/picnic-app/backend/role-api/internal/model"
)

type ReadActions interface {
	GetRole(ctx context.Context, itemID string) (model.Role, bool, error)
	GetRoles(ctx context.Context, itemIDs []string) ([]model.Role, error)
	GetAssignedRole(
		ctx context.Context,
		entityID, roleID, circleID, userID string) (
		model.AssignedRole, bool, error)
	GetAssignedRoles(
		ctx context.Context,
		entityID, circleID, userID string) (
		[]model.AssignedRole, error)
	GetCircleAssignedRoles(
		ctx context.Context,
		entityID string,
		circleIDs ...string) (
		[]model.AssignedRole, error)
}

type WriteActions interface {
	InsertRole(ctx context.Context, in model.Role) error
	UpdateRole(ctx context.Context, in model.Role) error
	PartialUpdateRoles(ctx context.Context, itemIDs []string, in model.RoleUpdate) error
	InsertAssignedRole(ctx context.Context, in model.AssignedRole) error
	DeleteAssignedRoles(ctx context.Context, itemIDs ...string) error
}

type ReadWriteActions interface {
	ReadActions
	WriteActions
}

type Repo interface {
	SingleRead() ReadActions
	SingleWrite() WriteActions
	ReadOnlyTx(context.Context, func(ctx context.Context, tx ReadActions) error) error
	ReadWriteTx(context.Context, func(ctx context.Context, tx ReadWriteActions) error) (time.Time, error)
}
