package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"github.com/Masterminds/squirrel"
	"google.golang.org/grpc/codes"

	"gitlab.com/picnic-app/backend/role-api/internal/model"
	"gitlab.com/picnic-app/backend/role-api/internal/repo"
	"gitlab.com/picnic-app/backend/role-api/internal/repo/spanner/helpers"
	"gitlab.com/picnic-app/backend/role-api/internal/repo/spanner/tables"
)

func (w writeOnly) InsertAssignedRole(
	ctx context.Context,
	in model.AssignedRole,
) error {
	return InsertAssignedRole(ctx, w.bufferWriter(ctx), in)
}

func (rw readWrite) InsertAssignedRole(
	ctx context.Context,
	in model.AssignedRole,
) error {
	return InsertAssignedRole(ctx, rw.tx, in)
}

func InsertAssignedRole(
	_ context.Context,
	db helpers.BufferWriter,
	in model.AssignedRole,
) error {
	var table tables.AssignedRoles

	in.CreatedAt = spanner.CommitTimestamp

	m, err := spanner.InsertStruct(
		table.TableName(),
		in,
	)
	if err != nil {
		return fromError(err)
	}

	return db.BufferWrite([]*spanner.Mutation{m})
}

func (r readOnly) GetAssignedRole(
	ctx context.Context,
	entityID, roleID, circleID, userID string,
) (model.AssignedRole, bool, error) {
	return GetAssignedRole(ctx, r.tx, entityID, roleID, circleID, userID)
}

func (rw readWrite) GetAssignedRole(
	ctx context.Context,
	entityID, roleID, circleID, userID string,
) (model.AssignedRole, bool, error) {
	return GetAssignedRole(ctx, rw.tx, entityID, roleID, circleID, userID)
}

func GetAssignedRole(ctx context.Context,
	db helpers.Queryer,
	entityID, roleID, circleID, userID string,
) (model.AssignedRole, bool, error) {
	table := tables.Get().AssignedRoles()

	builder := squirrel.
		Select(table.Columns()...).
		From(table.TableName()).
		Where(squirrel.Eq{table.EntityID(): entityID}).
		Where(squirrel.Eq{table.RoleID(): roleID})

	if circleID != "" {
		builder = builder.Where(squirrel.Eq{table.CircleID(): circleID})
	}

	if userID != "" {
		builder = builder.Where(squirrel.Eq{table.UserID(): userID})
	}

	q, args, err := builder.PlaceholderFormat(squirrel.AtP).ToSql()
	if err != nil {
		return model.AssignedRole{}, false, fromError(err)
	}

	params := helpers.ArgsToParams(args)
	stmt := spanner.Statement{SQL: q, Params: params}

	out, err := helpers.GetResult[model.AssignedRole](db.Query(ctx, stmt))
	if err != nil {
		if spanner.ErrCode(err) == codes.NotFound {
			return model.AssignedRole{}, false, nil
		}
		return model.AssignedRole{}, false, fromError(err)
	}

	return out, true, nil
}

func (r readOnly) GetAssignedRoles(
	ctx context.Context,
	entityID, circleID, userID string,
) ([]model.AssignedRole, error) {
	return GetAssignedRoles(ctx, r.tx, entityID, circleID, userID)
}

func (rw readWrite) GetAssignedRoles(
	ctx context.Context,
	entityID, circleID, userID string,
) ([]model.AssignedRole, error) {
	return GetAssignedRoles(ctx, rw.tx, entityID, circleID, userID)
}

func GetAssignedRoles(ctx context.Context,
	db helpers.Queryer,
	entityID, circleID, userID string,
) ([]model.AssignedRole, error) {
	table := tables.Get().AssignedRoles()

	builder := squirrel.
		Select(table.Columns()...).
		From(table.TableName()).
		Where(squirrel.Eq{table.EntityID(): entityID})

	if circleID != "" {
		builder = builder.Where(squirrel.Eq{table.CircleID(): circleID})
	}

	if userID != "" {
		builder = builder.Where(squirrel.Eq{table.UserID(): userID})
	}

	q, args, err := builder.PlaceholderFormat(squirrel.AtP).ToSql()
	if err != nil {
		return nil, fromError(err)
	}

	params := helpers.ArgsToParams(args)
	stmt := spanner.Statement{SQL: q, Params: params}

	out, err := helpers.GetResults[model.AssignedRole](db.Query(ctx, stmt))
	if err != nil {
		return nil, fromError(err)
	}

	return out, nil
}

func (r readOnly) GetCircleAssignedRoles(
	ctx context.Context,
	entityID string,
	circleIDs ...string,
) ([]model.AssignedRole, error) {
	return GetCircleAssignedRoles(ctx, r.tx, entityID, circleIDs...)
}

func (rw readWrite) GetCircleAssignedRoles(
	ctx context.Context,
	entityID string,
	circleIDs ...string,
) ([]model.AssignedRole, error) {
	return GetCircleAssignedRoles(ctx, rw.tx, entityID, circleIDs...)
}

func GetCircleAssignedRoles(
	ctx context.Context,
	db helpers.Queryer,
	entityID string,
	circleIDs ...string,
) ([]model.AssignedRole, error) {
	table := tables.Get().AssignedRoles()

	builder := squirrel.
		Select(table.Columns()...).
		From(table.TableName()).
		Where(squirrel.Eq{table.EntityID(): entityID}).
		Where(squirrel.Eq{table.CircleID(): circleIDs})

	q, args, err := builder.PlaceholderFormat(squirrel.AtP).ToSql()
	if err != nil {
		return nil, fromError(err)
	}

	params := helpers.ArgsToParams(args)
	stmt := spanner.Statement{SQL: q, Params: params}

	out, err := helpers.GetResults[model.AssignedRole](db.Query(ctx, stmt))
	if err != nil {
		return nil, fromError(err)
	}

	return out, nil
}

func (w writeOnly) DeleteAssignedRoles(
	ctx context.Context,
	itemIDs ...string,
) error {
	return w.tx(ctx, func(ctx context.Context, tx repo.WriteActions) error {
		return tx.DeleteAssignedRoles(ctx, itemIDs...)
	})
}

func (rw readWrite) DeleteAssignedRoles(
	ctx context.Context,
	itemIDs ...string,
) error {
	return DeleteAssignedRoles(ctx, rw.tx, itemIDs...)
}

func DeleteAssignedRoles(
	_ context.Context,
	db helpers.DBWriter,
	itemIDs ...string,
) error {
	err := helpers.Delete[tables.AssignedRoles](db, itemIDs...)
	if err != nil {
		return fromError(err)
	}

	return nil
}
