package data

import (
	"time"

	"cloud.google.com/go/spanner"
)

type Role struct {
	ID          string
	Name        string
	Permissions spanner.NullJSON
	CreatedAt   time.Time
	UpdatedAt   *time.Time
	DeletedAt   *time.Time
}
