package deserialize

import (
	"gitlab.com/picnic-app/backend/role-api/internal/model"
)

func Permission(in map[string]interface{}) model.Permission {
	permission := model.Permission{}

	if in == nil {
		return permission
	}

	for k, v := range in {
		switch k {
		case "Service":
			permission.Service, _ = v.(string)
		case "Operation":
			permission.Operation, _ = v.(string)
		case "Scope":
			permission.Scope, _ = v.(string)
		}
	}

	return permission
}
