package deserialize

import (
	"gitlab.com/picnic-app/backend/role-api/internal/model"
	"gitlab.com/picnic-app/backend/role-api/internal/repo/spanner/data"
)

func Role(in data.Role) model.Role {
	role := model.Role{
		ID:        in.ID,
		Name:      in.Name,
		CreatedAt: in.CreatedAt,
		UpdatedAt: in.UpdatedAt,
		DeletedAt: in.DeletedAt,
	}

	if in.Permissions.Valid {
		for _, v := range in.Permissions.Value.([]interface{}) {
			role.Permissions = append(role.Permissions, Permission(v.(map[string]interface{})))
		}
	}

	return role
}

func Roles(in []data.Role) []model.Role {
	result := make([]model.Role, len(in))
	for i, r := range in {
		result[i] = Role(r)
	}
	return result
}
