package spanner

import (
	"cloud.google.com/go/spanner"
	"google.golang.org/grpc/codes"

	"gitlab.com/picnic-app/backend/role-api/internal/errors"
)

func fromError(err error) error {
	if err == nil {
		return nil
	}

	if _, ok := err.(errors.Error); ok {
		return err
	}

	innerCode := spanner.ErrCode(err)
	code := codes.Internal
	switch innerCode {
	case codes.NotFound:
		fallthrough
	case codes.AlreadyExists:
		code = innerCode
	}

	return errors.NewWithError(code, "repo failed", err)
}
