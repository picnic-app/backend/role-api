package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"google.golang.org/api/option"

	"gitlab.com/picnic-app/backend/libs/golang/config"
)

func NewRepo(ctx context.Context, cfg config.Spanner) (Repo, error) {
	cli, err := NewSpannerClient(ctx, cfg.DSN, cfg.ADCPath)
	if err != nil {
		return Repo{}, fromError(err)
	}

	return Repo{
		cli: cli,
	}, nil
}

func NewRepoWithClient(cli *spanner.Client) Repo { return Repo{cli: cli} }

type Repo struct{ cli *spanner.Client }

func (r Repo) Close() { r.cli.Close() }

func NewSpannerClient(ctx context.Context, dsn, adcCredPath string) (*spanner.Client, error) {
	var opts []option.ClientOption
	if adcCredPath != "" {
		opts = append(opts, option.WithCredentialsFile(adcCredPath))
	}

	client, err := spanner.NewClient(ctx, dsn, opts...)
	if err != nil {
		return nil, fromError(err)
	}

	return client, nil
}
