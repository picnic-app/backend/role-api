package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"google.golang.org/grpc/codes"

	"gitlab.com/picnic-app/backend/role-api/internal/model"
	"gitlab.com/picnic-app/backend/role-api/internal/repo/spanner/data"
	"gitlab.com/picnic-app/backend/role-api/internal/repo/spanner/deserialize"
	"gitlab.com/picnic-app/backend/role-api/internal/repo/spanner/helpers"
	"gitlab.com/picnic-app/backend/role-api/internal/repo/spanner/serialize"
	"gitlab.com/picnic-app/backend/role-api/internal/repo/spanner/tables"
)

func (w writeOnly) InsertRole(ctx context.Context, in model.Role) error {
	return InsertRole(ctx, w.bufferWriter(ctx), in)
}

func (rw readWrite) InsertRole(ctx context.Context, in model.Role) error {
	return InsertRole(ctx, rw.tx, in)
}

func InsertRole(_ context.Context, db helpers.BufferWriter, in model.Role) error {
	var table tables.Roles

	in.CreatedAt = spanner.CommitTimestamp

	m, err := spanner.InsertStruct(
		table.TableName(),
		serialize.Role(in),
	)
	if err != nil {
		return fromError(err)
	}

	err = db.BufferWrite([]*spanner.Mutation{m})
	if err != nil {
		return fromError(err)
	}

	return nil
}

func (r readOnly) GetRole(ctx context.Context, itemID string) (model.Role, bool, error) {
	return GetRole(ctx, r.tx, itemID)
}

func (rw readWrite) GetRole(ctx context.Context, itemID string) (model.Role, bool, error) {
	return GetRole(ctx, rw.tx, itemID)
}

func GetRole(ctx context.Context, db helpers.RowReader, itemID string) (model.Role, bool, error) {
	item, err := helpers.GetByKey[data.Role, tables.Roles](ctx, db, itemID)
	if err != nil {
		if spanner.ErrCode(err) == codes.NotFound {
			return model.Role{}, false, nil
		}

		return model.Role{}, false, fromError(err)
	}

	return deserialize.Role(item), true, nil
}

func (r readOnly) GetRoles(ctx context.Context, itemIDs []string) ([]model.Role, error) {
	return GetRoles(ctx, r.tx, itemIDs)
}

func (rw readWrite) GetRoles(ctx context.Context, itemIDs []string) ([]model.Role, error) {
	return GetRoles(ctx, rw.tx, itemIDs)
}

func GetRoles(ctx context.Context, db helpers.Reader, itemIDs []string) ([]model.Role, error) {
	items, err := helpers.GetByKeys[data.Role, tables.Roles](ctx, db, itemIDs...)
	if err != nil {
		return nil, fromError(err)
	}

	return deserialize.Roles(items), nil
}

func (w writeOnly) UpdateRole(ctx context.Context, in model.Role) error {
	return UpdateRole(ctx, w.bufferWriter(ctx), in)
}

func (rw readWrite) UpdateRole(ctx context.Context, in model.Role) error {
	return UpdateRole(ctx, rw.tx, in)
}

func UpdateRole(_ context.Context, db helpers.BufferWriter, in model.Role) error {
	var table tables.Roles
	m, err := spanner.UpdateStruct(
		table.TableName(),
		serialize.Role(in),
	)
	if err != nil {
		return fromError(err)
	}

	err = db.BufferWrite([]*spanner.Mutation{m})
	if err != nil {
		return fromError(err)
	}

	return nil
}

func (w writeOnly) PartialUpdateRoles(ctx context.Context, itemIDs []string, in model.RoleUpdate) error {
	return PartialUpdateRoles(ctx, w.bufferWriter(ctx), itemIDs, in)
}

func (rw readWrite) PartialUpdateRoles(ctx context.Context, itemIDs []string, in model.RoleUpdate) error {
	return PartialUpdateRoles(ctx, rw.tx, itemIDs, in)
}

func PartialUpdateRoles(_ context.Context, db helpers.BufferWriter, itemIDs []string, in model.RoleUpdate) error {
	var table tables.Roles

	updates := make([]*spanner.Mutation, len(itemIDs))

	for i, itemID := range itemIDs {
		columns := []string{table.ID()}
		values := []any{itemID}

		if in.Name != nil {
			columns = append(columns, table.Name())
			values = append(values, in.Name.Value)
		}

		if in.Permissions != nil {
			columns = append(columns, table.Permissions())
			values = append(values, serialize.Permissions(in.Permissions.Value.([]model.Permission)))
		}

		if in.DeletedAt != nil {
			columns = append(columns, table.DeletedAt())
			values = append(values, in.DeletedAt.Value)
		}

		updates[i] = spanner.Update(
			table.TableName(),
			columns,
			values,
		)

	}

	err := db.BufferWrite(updates)
	if err != nil {
		return fromError(err)
	}

	return nil
}
