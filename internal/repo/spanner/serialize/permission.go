package serialize

import (
	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/role-api/internal/model"
)

func Permissions(in []model.Permission) spanner.NullJSON {
	return spanner.NullJSON{
		Valid: true,
		Value: in,
	}
}
