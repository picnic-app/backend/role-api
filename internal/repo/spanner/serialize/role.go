package serialize

import (
	"gitlab.com/picnic-app/backend/role-api/internal/model"
	"gitlab.com/picnic-app/backend/role-api/internal/repo/spanner/data"
)

func Role(in model.Role) data.Role {
	return data.Role{
		ID:          in.ID,
		Name:        in.Name,
		Permissions: Permissions(in.Permissions),
		CreatedAt:   in.CreatedAt,
		UpdatedAt:   in.UpdatedAt,
		DeletedAt:   in.DeletedAt,
	}
}
