package tables

func (t AllTables) AssignedRoles() AssignedRoles { return AssignedRoles(t) }

type AssignedRoles struct{ alias string }

func (a AssignedRoles) TableAlias() string { return a.alias }
func (a AssignedRoles) TableName() string  { return aliasedTable(a.alias, "AssignedRoles") }
func (a AssignedRoles) Columns() []string {
	return []string{
		a.ID(),
		a.EntityID(),
		a.RoleID(),
		a.CircleID(),
		a.UserID(),
		a.CreatedAt(),
	}
}

func (a AssignedRoles) ID() string        { return aliasedCol(a.alias, "ID") }
func (a AssignedRoles) EntityID() string  { return aliasedCol(a.alias, "EntityID") }
func (a AssignedRoles) RoleID() string    { return aliasedCol(a.alias, "RoleID") }
func (a AssignedRoles) CircleID() string  { return aliasedCol(a.alias, "CircleID") }
func (a AssignedRoles) UserID() string    { return aliasedCol(a.alias, "UserID") }
func (a AssignedRoles) CreatedAt() string { return aliasedCol(a.alias, "CreatedAt") }
