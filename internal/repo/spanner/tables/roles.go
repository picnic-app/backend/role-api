package tables

func (t AllTables) Roles() Roles { return Roles(t) }

type Roles struct{ alias string }

func (a Roles) TableAlias() string { return a.alias }
func (a Roles) TableName() string  { return aliasedTable(a.alias, "Roles") }
func (a Roles) Columns() []string {
	return []string{
		a.ID(),
		a.Name(),
		a.Permissions(),
		a.CreatedAt(),
		a.UpdatedAt(),
		a.DeletedAt(),
	}
}

func (a Roles) ID() string          { return aliasedCol(a.alias, "ID") }
func (a Roles) Name() string        { return aliasedCol(a.alias, "Name") }
func (a Roles) Permissions() string { return aliasedCol(a.alias, "Permissions") }
func (a Roles) CreatedAt() string   { return aliasedCol(a.alias, "CreatedAt") }
func (a Roles) UpdatedAt() string   { return aliasedCol(a.alias, "UpdatedAt") }
func (a Roles) DeletedAt() string   { return aliasedCol(a.alias, "DeletedAt") }
