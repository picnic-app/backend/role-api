package tables

type Table interface {
	TableName() string
	Columns() []string
}

func Get() AllTables { return AllTables{} }

func WithAlias(alias string) AllTables { return AllTables{alias: alias} }

type AllTables struct{ alias string }

func aliasedTable(alias, table string) string {
	if alias == "" {
		return table
	}
	return table + " AS " + alias
}

func aliasedCol(alias, col string) string {
	if alias == "" {
		return col
	}
	return alias + "." + col
}
