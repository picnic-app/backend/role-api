package spanner

import (
	"context"
	"time"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/role-api/internal/repo"
)

type (
	readOnly  struct{ tx *spanner.ReadOnlyTransaction }
	writeOnly struct{ cli *spanner.Client }
	readWrite struct{ tx *spanner.ReadWriteTransaction }
)

var (
	_ repo.ReadActions      = readOnly{}
	_ repo.WriteActions     = writeOnly{}
	_ repo.ReadWriteActions = readWrite{}
)

func (r Repo) SingleRead() repo.ReadActions { return readOnly{tx: r.cli.Single()} }

func (r Repo) SingleWrite() repo.WriteActions { return writeOnly(r) }

func (r Repo) ReadOnlyTx(ctx context.Context, f func(context.Context, repo.ReadActions) error) error {
	tx := r.cli.ReadOnlyTransaction()
	defer tx.Close()

	err := f(ctx, readOnly{tx: tx})
	if err != nil {
		return fromError(err)
	}

	return nil
}

func (r Repo) ReadWriteTx(
	ctx context.Context,
	f func(context.Context, repo.ReadWriteActions) error,
) (time.Time, error) {
	ts, err := r.cli.ReadWriteTransaction(ctx, func(ctx context.Context, tx *spanner.ReadWriteTransaction) error {
		return f(ctx, readWrite{tx: tx})
	})
	if err != nil {
		return ts, fromError(err)
	}

	return ts, nil
}

func (w writeOnly) tx(ctx context.Context, f func(ctx context.Context, tx repo.WriteActions) error) error {
	_, err := w.cli.ReadWriteTransaction(ctx, func(ctx context.Context, tx *spanner.ReadWriteTransaction) error {
		return f(ctx, readWrite{tx: tx})
	})
	return fromError(err)
}

func (w writeOnly) bufferWriter(ctx context.Context, opts ...spanner.ApplyOption) bufferWriter {
	return bufferWriter{ctx: ctx, cli: w.cli, opts: opts}
}

type bufferWriter struct {
	ctx  context.Context
	cli  *spanner.Client
	opts []spanner.ApplyOption
}

func (u bufferWriter) BufferWrite(m []*spanner.Mutation) error {
	_, err := u.cli.Apply(u.ctx, m, u.opts...)
	return fromError(err)
}
