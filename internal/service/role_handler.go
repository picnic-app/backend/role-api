package service

import (
	"context"
	"strings"
	"time"

	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	"gitlab.com/picnic-app/backend/role-api/internal/errors"
	"gitlab.com/picnic-app/backend/role-api/internal/model"
	"gitlab.com/picnic-app/backend/role-api/internal/repo"
	"gitlab.com/picnic-app/backend/role-api/internal/util/slice"
)

const (
	NoneScope = "-"
	AnyScope  = "*"
)

type RoleHandler struct {
	repo repo.Repo
}

func NewRoleHandler(repo repo.Repo) RoleHandler {
	return RoleHandler{
		repo: repo,
	}
}

func (h RoleHandler) CreateRole(ctx context.Context, role model.Role) (model.Role, error) {
	role.ID = uuid.NewString()

	err := h.repo.SingleWrite().InsertRole(ctx, role)
	if err != nil {
		return model.Role{}, err
	}

	out, found, err := h.repo.SingleRead().GetRole(ctx, role.ID)
	if err != nil {
		return model.Role{}, err
	}
	if !found {
		return model.Role{}, errors.InternalError(errors.NotFoundError("role"))
	}

	return out, err
}

func (h RoleHandler) GetRole(ctx context.Context, roleID string) (model.Role, error) {
	if roleID == "" {
		return model.Role{}, errors.InvalidArgumentError("roleID")
	}

	role, found, err := h.repo.SingleRead().GetRole(ctx, roleID)
	if err != nil {
		return model.Role{}, err
	}
	if !found || role.DeletedAt != nil {
		return model.Role{}, errors.NotFoundError("role")
	}

	return role, nil
}

func (h RoleHandler) GetRoles(ctx context.Context, roleIDs []string) ([]model.Role, error) {
	if len(roleIDs) == 0 {
		return nil, errors.InvalidArgumentError("roleIDs")
	}

	return h.repo.SingleRead().GetRoles(ctx, roleIDs)
}

func (h RoleHandler) UpdateRole(ctx context.Context, role model.Role) (model.Role, error) {
	if role.ID == "" {
		return model.Role{}, errors.InvalidArgumentError("ID")
	}

	err := h.repo.SingleWrite().UpdateRole(ctx, role)
	if err != nil {
		return model.Role{}, err
	}

	out, found, err := h.repo.SingleRead().GetRole(ctx, role.ID)
	if err != nil {
		return model.Role{}, err
	}
	if !found {
		return model.Role{}, errors.InternalError(errors.NotFoundError("role"))
	}

	return out, err
}

func (h RoleHandler) RemovePermissions(ctx context.Context, roleID string, permissions []model.Permission) error {
	if roleID == "" {
		return errors.InvalidArgumentError("roleID")
	}

	if len(permissions) == 0 {
		return errors.InvalidArgumentError("permissions")
	}

	_, err := h.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		role, found, err := tx.GetRole(ctx, roleID)
		if err != nil {
			return err
		}
		if !found {
			return errors.NotFoundError("role")
		}

		var out []model.Permission
		for _, rolePermission := range role.Permissions {
			remove := false
			for _, permission := range permissions {
				if rolePermission.Service != permission.Service || rolePermission.Operation != permission.Operation {
					continue
				}
				remove = true
				break
			}

			if remove {
				continue
			}

			out = append(out, rolePermission)
		}

		role.Permissions = out

		return tx.UpdateRole(ctx, role)
	})

	return err
}

func (h RoleHandler) DeleteRoles(ctx context.Context, roleIDs ...string) error {
	if len(roleIDs) == 0 {
		return errors.InvalidArgumentError("roleIDs")
	}

	err := h.repo.SingleWrite().PartialUpdateRoles(ctx, roleIDs, model.RoleUpdate{
		DeletedAt: &model.Field{Value: time.Now().UTC()},
	})
	if err != nil {
		return err
	}

	return err
}

func (h RoleHandler) AssignRole(ctx context.Context, entityID, roleID, circleID, userID string) error {
	if entityID == "" {
		return errors.InvalidArgumentError("entityID")
	}

	if roleID == "" {
		return errors.InvalidArgumentError("roleID")
	}

	role := model.AssignedRole{
		ID:       uuid.NewString(),
		EntityID: entityID,
		RoleID:   roleID,
		CircleID: circleID,
		UserID:   userID,
	}

	return h.repo.SingleWrite().InsertAssignedRole(ctx, role)
}

func (h RoleHandler) GetAssignedRoles(
	ctx context.Context,
	entityID, circleID, userID string) (
	[]model.AssignedRole, error,
) {
	if entityID == "" {
		return nil, errors.InvalidArgumentError("entityID")
	}

	return h.repo.SingleRead().GetAssignedRoles(ctx, entityID, circleID, userID)
}

func (h RoleHandler) RevokeRole(ctx context.Context, entityID, roleID, circleID, userID string) error {
	if entityID == "" {
		return errors.InvalidArgumentError("entityID")
	}

	if roleID == "" {
		return errors.InvalidArgumentError("roleID")
	}

	assignedRole, found, err := h.repo.SingleRead().GetAssignedRole(ctx, entityID, roleID, circleID, userID)
	if err != nil {
		return err
	}
	if !found {
		return errors.NotFoundError("assigned role")
	}

	return h.repo.SingleWrite().DeleteAssignedRoles(ctx, assignedRole.ID)
}

func (h RoleHandler) RevokeRoles(ctx context.Context, entityID, circleID, userID string) error {
	if entityID == "" {
		return errors.InvalidArgumentError("entityID")
	}

	assignedRoles, err := h.repo.SingleRead().GetAssignedRoles(ctx, entityID, circleID, userID)
	if err != nil {
		return err
	}

	ids := make([]string, len(assignedRoles))
	for i, assignedRole := range assignedRoles {
		ids[i] = assignedRole.ID
	}

	return h.repo.SingleWrite().DeleteAssignedRoles(ctx, ids...)
}

func (h RoleHandler) Authorize(
	ctx context.Context,
	service, operation string,
	circleIDs []string,
	userIDs []string,
) (map[string]string, error) {
	userID, _ := auth.GetUserID(ctx)
	appID := auth.GetAppID(ctx)

	if appID == "" && userID == "" {
		return nil, status.Error(codes.Unauthenticated, "unauthenticated")
	}

	if appID == "" {
		return nil, status.Error(codes.Unimplemented, "unimplemented")
	}

	if len(userIDs) > 0 {
		return nil, status.Error(codes.Unimplemented, "unimplemented")
	}

	if service == "" {
		return nil, status.Error(codes.InvalidArgument, "service")
	}

	if operation == "" {
		return nil, status.Error(codes.InvalidArgument, "operation")
	}

	if len(circleIDs) == 0 {
		return nil, status.Error(codes.InvalidArgument, "circleIDs")
	}

	circleIDs = slice.Unique(circleIDs)

	assignedRoles, err := h.repo.SingleRead().GetCircleAssignedRoles(ctx, appID, circleIDs...)
	if err != nil {
		return nil, err
	}

	roleIDs := slice.UniqueValues(assignedRoles, func(assignedRole model.AssignedRole) string {
		return assignedRole.RoleID
	})
	roles, err := h.repo.SingleRead().GetRoles(ctx, roleIDs)
	if err != nil {
		return nil, err
	}

	scopes := map[string]string{}
	for _, circleID := range circleIDs {
		assignedRoles := slice.Filter(assignedRoles, func(assignedRole model.AssignedRole) bool {
			return assignedRole.CircleID == circleID
		})
		if len(assignedRoles) == 0 {
			return nil, status.Error(codes.PermissionDenied, "unauthorized")
		}

		circleScopes := make([]string, 0, len(assignedRoles))
		for _, assignedRole := range assignedRoles {
			roles := slice.Filter(roles, func(role model.Role) bool {
				return role.ID == assignedRole.RoleID
			})
			if len(roles) == 0 {
				continue
			}

			permissions := slice.Filter(roles[0].Permissions, func(permission model.Permission) bool {
				return strings.EqualFold(service, permission.Service) && strings.EqualFold(operation, permission.Operation)
			})
			if len(permissions) == 0 {
				continue
			}

			for _, permission := range permissions {
				circleScopes = append(circleScopes, permission.Scope)
			}
		}

		if len(circleScopes) == 0 {
			return nil, status.Error(codes.PermissionDenied, "unauthorized")
		}

		merged := mergeScopes(circleScopes)
		if merged == NoneScope {
			return nil, status.Error(codes.PermissionDenied, "unauthorized")
		}

		scopes[circleID] = mergeScopes(circleScopes)
	}

	// TODO handle user identity granted roles for composite context,
	// once we move user level authorization to the role api

	return scopes, nil
}

func mergeScopes(scopes []string) string {
	if len(scopes) == 1 {
		return scopes[0]
	}

	filtered := slice.Filter(scopes, func(scope string) bool {
		return scope == NoneScope
	})
	if len(filtered) > 0 {
		return NoneScope
	}

	filtered = slice.Filter(scopes, func(scope string) bool {
		return scope == AnyScope
	})
	if len(filtered) > 0 {
		return AnyScope
	}

	return strings.Join(scopes, " AND ")
}
