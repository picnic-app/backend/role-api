package service

import (
	"gitlab.com/picnic-app/backend/role-api/internal/repo"
)

func New(
	repo repo.Repo,
) *Service {
	return &Service{
		RoleHandler: NewRoleHandler(repo),
	}
}

type Service struct {
	RoleHandler
}
