package background

import (
	"context"
	"time"

	"go.uber.org/zap"

	"gitlab.com/picnic-app/backend/libs/golang/logger"
)

// Context copies all values from the context. Such a context can
// be used to run background tasks, as it does not contain deadlines and cannot
// be canceled by the parent context.
func Context(ctx context.Context) context.Context {
	log := logger.FromContext(ctx).With(zap.Bool("background", true))
	ctx = logger.ToContext(ctx, log)
	return &copyValuesCtx{ctx: ctx}
}

type copyValuesCtx struct{ ctx context.Context }

func (*copyValuesCtx) Err() error                       { return nil }
func (*copyValuesCtx) Done() <-chan struct{}            { return nil }
func (*copyValuesCtx) Deadline() (v time.Time, ok bool) { return v, ok }
func (c *copyValuesCtx) Value(key any) any              { return c.ctx.Value(key) }
