package background_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/role-api/internal/util/background"
)

func TestBackgroundContextFrom(t *testing.T) {
	var key struct{}
	const value = 1
	withValue := context.WithValue(context.Background(), key, value)

	canceled, cancel := context.WithCancel(withValue)
	cancel()

	copied := background.Context(canceled)
	require.NoError(t, copied.Err())

	deadline, ok := copied.Deadline()
	require.False(t, ok)
	require.Zero(t, deadline)

	select {
	case <-copied.Done():
		t.Fatal("should not be done")
	default:
	}

	require.Equal(t, withValue.Value(key), copied.Value(key))
}
