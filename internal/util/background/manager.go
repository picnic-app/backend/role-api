package background

import (
	"context"
	"sync"
)

type key struct{}

type Manager struct{ wg sync.WaitGroup }

func NotifyWhenCompleted(ctx context.Context) (context.Context, <-chan struct{}) {
	ch := make(chan struct{})
	return context.WithValue(ctx, key{}, ch), ch
}

func (m *Manager) RunBackgroundTask(ctx context.Context, f func(context.Context)) {
	m.wg.Add(1)
	go func() {
		defer m.wg.Done()
		f(Context(ctx))

		if done, _ := ctx.Value(key{}).(chan struct{}); done != nil {
			done <- struct{}{}
		}
	}()
}

func (m *Manager) Wait() { m.wg.Wait() }
