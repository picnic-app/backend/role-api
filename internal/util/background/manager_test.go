package background_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/role-api/internal/util/background"
)

func TestManager(t *testing.T) {
	var m background.Manager

	ctx, done := background.NotifyWhenCompleted(context.Background())

	start := time.Now()
	m.RunBackgroundTask(ctx, func(ctx context.Context) { time.Sleep(time.Millisecond) })
	<-done
	m.Wait()
	require.GreaterOrEqual(t, time.Since(start).Milliseconds(), int64(1))
}
