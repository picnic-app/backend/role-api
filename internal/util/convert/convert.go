package convert

import "cloud.google.com/go/spanner"

func ToPointer[T any](item T) *T {
	return &item
}

func ToPointers[T any](items []T) []*T {
	var out []*T
	for _, item := range items {
		item := item
		out = append(out, &item)
	}

	return out
}

func ToValues[T any](items []*T) []T {
	var out []T
	for _, item := range items {
		out = append(out, *item)
	}

	return out
}

func ToNullString(item string) spanner.NullString {
	return spanner.NullString{StringVal: item, Valid: item != ""}
}
