package grpcclient

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/picnic-app/backend/libs/golang/core/mw"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
	"gitlab.com/picnic-app/backend/libs/golang/monitoring/tracing"
)

type (
	ctxAppNameKey struct{}
)

func New(ctx context.Context, host string, port int, secure bool, serviceName, secret string) *grpc.ClientConn {
	addr := fmt.Sprintf("%s:%d", host, port)
	cred := insecure.NewCredentials()

	if secure {
		systemRoots, err := x509.SystemCertPool()
		if err != nil {
			logger.Fatal(ctx, err)
		}
		cred = credentials.NewTLS(&tls.Config{
			MinVersion: tls.VersionTLS12,
			MaxVersion: tls.VersionTLS13,
			RootCAs:    systemRoots,
		})
	}

	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(cred),
		grpc.WithChainUnaryInterceptor(
			mw.NewClientContextInterceptor(serviceName, secret).Unary(),
			tracing.ClientUnaryInterceptor(),
		),
		grpc.WithChainStreamInterceptor(
			mw.NewClientContextInterceptor(serviceName, secret).Stream(),
			tracing.ClientStreamInterceptor(),
		),
		grpc.WithUserAgent(serviceName),
	}

	conn, err := grpc.DialContext(context.WithValue(ctx, ctxAppNameKey{}, serviceName), addr, opts...)
	if err != nil {
		log.Fatalf("failed to create grpc client: %v", err)
	}
	return conn
}
