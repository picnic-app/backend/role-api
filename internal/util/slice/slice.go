package slice

import "github.com/pkg/errors"

func Batch[T any](s []T, batchSize int, f func(batch []T) error) error {
	if batchSize <= 0 {
		return nil
	}

	for start := 0; start < len(s); start += batchSize {
		end := min(start+batchSize, len(s))
		err := f(s[start:end])
		if err != nil {
			return errors.Wrapf(err, "batch processing %T from %d to %d", s, start, end)
		}
	}
	return nil
}

func Filter[T any](s []T, ok func(v T) bool) []T {
	var out []T
	for _, v := range s {
		if ok(v) {
			out = append(out, v)
		}
	}
	return out
}

// Reorder sorts s in the order of keys. The values in keys must be unique, but
// the function is tolerant of non-unique values in s. Values from s not present
// in keys will be skipped.
func Reorder[T any, K comparable](keys []K, s []T, key func(obj T) K) []T {
	m, n := make(map[K][]T, len(s)), 0
	for i, v := range s {
		k := key(v)
		if tmp := m[k]; len(tmp) == 0 {
			m[k] = s[i : i+1 : i+1] // To avoid allocations for unique elements.
		} else {
			m[k], n = append(tmp, v), n+1
		}
	}

	s = make([]T, 0, len(keys)+n)
	for _, key := range keys {
		if v, ok := m[key]; ok {
			s = append(s, v...)
		}
	}
	return s
}

func ValuesByKey[T, K any](s []T, key func(obj T) K) []K {
	v := make([]K, len(s))
	for i, obj := range s {
		v[i] = key(obj)
	}
	return v
}

func UniqueValues[T any, K comparable](s []T, key func(obj T) K) []K {
	v := make([]K, len(s))
	for i, obj := range s {
		v[i] = key(obj)
	}
	return Unique(v)
}

func Convert[A, B any](f func(A) B, a ...A) []B {
	result := make([]B, len(a))
	for i, a := range a {
		result[i] = f(a)
	}
	return result
}

func ConvertPointers[A, B any](f func(A) B, a ...A) []*B {
	return Convert(func(a A) *B { v := f(a); return &v }, a...)
}

// Unique returns unique values from s. The function reuses s to store values, so
// s should not be used afterward. The order of the values is preserved.
func Unique[T comparable](s []T) []T {
	set, n, ok := make(map[T]struct{}, len(s)), 0, false
	for _, v := range s {
		if _, ok = set[v]; !ok {
			s[n], set[v], n = v, struct{}{}, n+1
		}
	}
	return s[:n]
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
