package slice_test

import (
	"fmt"
	"strconv"
	"testing"

	"gitlab.com/picnic-app/backend/role-api/internal/util/slice"
)

func ExampleBatch() {
	s := []int{1, 2, 3, 4, 5}
	_ = slice.Batch(s, 2, func(batch []int) error {
		fmt.Println(batch)
		return nil
	})

	// Output:
	// [1 2]
	// [3 4]
	// [5]
}

func ExampleFilter() {
	s := []int{1, 2, 3, 4, 5}
	s = slice.Filter(s, func(v int) bool { return v&1 == 1 })
	fmt.Println(s)

	// Output:
	// [1 3 5]
}

func ExampleConvert() {
	ints := []int{1, 2, 3}
	strings := slice.Convert(strconv.Itoa, ints...)
	fmt.Println(strings)

	// Output:
	// [1 2 3]
}

func ExampleConvertPointers() {
	ints := []int{1, 2, 3}
	strings := slice.ConvertPointers(strconv.Itoa, ints...)

	for _, p := range strings {
		fmt.Println(*p)
	}

	// Output:
	// 1
	// 2
	// 3
}

func ExampleReorder() {
	keys := []string{"1", "2", "3"}
	vals := []int{3, 2, 3, 2, 0}
	s := slice.Reorder(keys, vals, strconv.Itoa)
	fmt.Println(s)

	// Output:
	// [2 2 3 3]
}

func BenchmarkReorder_Unique(b *testing.B) {
	for i := 0; i < b.N; i++ {
		slice.Reorder(
			[]string{"1", "2", "3", "4"},
			[]int{3, 2, 1, 4},
			strconv.Itoa,
		)
	}
}

func ExampleUnique() {
	s := slice.Unique([]int{3, 2, 3, 2, 0})
	fmt.Println(s)

	// Output:
	// [3 2 0]
}
